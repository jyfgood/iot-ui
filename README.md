


## 介绍

本项目基于avue-cli手脚架开发，是一款基于avue和element-ui和ivew-UI完全开源、免费的企业后端产品前端集成解决方案，采用最新的前端技术栈，已经准备好了大部分的项目准备工作，你可以快速进行二次开发。购买全套源码400元+后端+配置文件
后端采用微服务技术可自由拓展那业务

## 文档

[文档](https://www.kancloud.cn/smallwei/avue/579870)
[文档说明](https://avuejs.com/doc/plugins/avue-cli)
[java后端](https://gitee.com/jyfgood/jyf-cloud-iot)
## 预览

[预览](https://cli2.avuejs.com)



菜单     
## 开发

```
# 克隆项目
git clone https://gitee.com/jyfgood/iot-ui.git

# 进入项目
cd avue-cli

# 安装依赖
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run serve

```
## 功能
- 全局错误日志记录
- vuex持久化存储
- 主题色切换
- 消息提醒
- 锁屏
- 数据展示
- 登录/注销
  - 用户名登录
  - 验证码登录
- 权限验证
- 第三方网站嵌套
- CRUD(增删改查)
- FORM(动态生成)
- 阿里巴巴图标库(在线调用)
- tag标签操作
- 环境变量
- 表格树（暂时使用iview-ui的表格，可能是版本问题导致element的表格树无法正常使用）
- 引导页
- 数据持久化
- 剪切板
- 灰度化
- 系统管理
  - 用户管理
  - 角色管理
  - 菜单管理
  - 字典管理
  - 岗位管理
- 高级路由
  - 动态路由
  - 参数路由
- 更多功能开在开发

## 问答

有关问题和支持，请使用[issues](https://gitee.com/jyfgood/iot-ui/issues)或加入QQ群.

## issues

打开问题之前，请务必提供详细的问题过程和截图，不符合准则的问题将会被拒绝.

## Changelog

每个版本的详细更改记录在[发行说明](https://gitee.com/jyfgood/iot-ui/releases).

## License
<p>给贡献代码的程序源 (媛)(猿)，买点瓜子饮料矿泉水 :smile:  :pray:  :pray:  :clap: </p>
<img  src='https://images.gitee.com/uploads/images/2020/0622/135050_e5c387c0_2671397.png' height="300" width="200">
<img  src='https://images.gitee.com/uploads/images/2020/0622/135418_c3da64a9_2671397.png' height="300" width="200">
<p>[MIT](http://opensource.org/licenses/MIT)</p>