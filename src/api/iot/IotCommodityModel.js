import request from '@/router/axios';
import {baseUrl} from '@/config/env';
//显示详细信息
export const list = (dto) => request({
    url: baseUrl +'/commodity/iotCommodityModel/list',
    method: 'post',
    data: dto
});

//修改详细信息
export const update = (obj) => request({
    url: baseUrl +'/commodity/iotCommodityModel/update',
    method: 'post',
    data: obj
});

//添加详细信息
export const add = (obj) => request({
    url: baseUrl +'/commodity/iotCommodityModel/add',
    method: 'post',
    data: obj
});
//删除详细信息
export const del = (id) => request({
    url: baseUrl +'/commodity/iotCommodityModel/delete/'+id,
    method: 'post'
});
