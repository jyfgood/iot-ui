import request from '@/router/axios';
import {baseUrl} from '@/config/env';
//显示详细信息
export const list = (dto) => request({
    url: baseUrl +'/pd/iotOrder/list',
    method: 'post',
    data: dto
});

//修改详细信息
export const update = (obj) => request({
    url: baseUrl +'/pd/iotOrder/update',
    method: 'post',
    data: obj
});

//添加详细信息
export const add = (obj) => request({
    url: baseUrl +'/pd/iotOrder/add',
    method: 'post',
    data: obj
});
//删除详细信息
export const del = (id) => request({
    url: baseUrl +'/pd/iotOrder/delete/'+id,
    method: 'post'
});
