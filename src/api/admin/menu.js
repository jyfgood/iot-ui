import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysMenu/delete/'+id,
    method: 'post',

})

export const update = (menu) => request({
    url: baseUrl + '/admin/sysMenu/update',
    method: 'post',
    data:menu
})

export const add = (menu) => request({
    url: baseUrl + '/admin/sysMenu/add',
    method: 'post',
    data:menu
})
export const save = (menu) => request({
    url: baseUrl + '/admin/sysMenu/save',
    method: 'post',
    data:menu
})
export const listLoad= (menu) => request({
    url: baseUrl + '/admin/sysMenu/list',
    method: 'post',
    data:menu
})

export const treeListLoad= (menu) => request({
    url: baseUrl + '/admin/sysMenu/treeList',
    method: 'POST',
    data:menu
})

