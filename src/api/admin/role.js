import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysRole/delete/'+id,
    method: 'post',

})

export const update = (dto) => request({
    url: baseUrl + '/admin/sysRole/update',
    method: 'post',
    data:menu
})

export const add = (dto) => request({
    url: baseUrl + '/admin/sysRole/add',
    method: 'post',
    data:menu
})

export const listLoad= (dto) => request({
    url: baseUrl + '/admin/sysRole/list',
    method: 'post',
    data:menu
})

export const roleMenuSetting= (dto) => request({
    url: baseUrl + '/admin/sysRole/roleMenuSetting',
    method: 'post',
    data:dto
})

export const getRoleAll= (dto) => request({
    url: baseUrl + '/admin/sysRole/roleAll',
    method: 'post',
    data:dto
})

