import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysPosition/delete/'+id,
    method: 'post',

})

export const update = (dto) => request({
    url: baseUrl + '/admin/sysPosition/update',
    method: 'post',
    data:dto
})

export const add = (dto) => request({
    url: baseUrl + '/admin/sysPosition/add',
    method: 'post',
    data:dto
})

export const listLoad= (dto) => request({
    url: baseUrl + '/admin/sysPosition/list',
    method: 'post',
    data:dto
})

export const getList= (dto) => request({
    url: baseUrl + '/admin/sysPosition/getList',
    method: 'post',
    data:dto
})

export const positionRoleSetting= (dto) => request({
    url: baseUrl + '/admin/sysPosition/positionRoleSetting',
    method: 'post',
    data:dto
})

