import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysDept/delete/'+id,
    method: 'post',

})

export const update = (dto) => request({
    url: baseUrl + '/admin/sysDept/update',
    method: 'post',
    data:dto
})

export const add = (dto) => request({
    url: baseUrl + '/admin/sysDept/add',
    method: 'post',
    data:dto
})

export const listLoad= (dto) => request({
    url: baseUrl + '/admin/sysDept/list',
    method: 'post',
    data:dto
})
export const getDeptTreeList= (dto) => request({
    url: baseUrl + '/admin/sysDept/treeList',
    method: 'post',
    data:dto
})




