import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysDict/delete/'+id,
    method: 'post',

})

export const update = (dto) => request({
    url: baseUrl + '/admin/sysDict/update',
    method: 'post',
    data:dto
})

export const add = (dto) => request({
    url: baseUrl + '/admin/sysDict/add',
    method: 'post',
    data:dto
})

export const listLoad= (dto) => request({
    url: baseUrl + '/admin/sysDict/list',
    method: 'post',
    data:dto
})

export const getDeptTreeList= (dto) => request({
    url: baseUrl + '/admin/sysDict/treeList',
    method: 'post',
    data:dto
})

export const getList= (dto) => request({
    url: baseUrl + '/admin/sysDict/getList',
    method: 'post',
    data:dto
})

