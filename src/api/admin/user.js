import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const del = (id) => request({
    url: baseUrl + '/admin/sysUser/delete/'+id,
    method: 'post',

})

export const update = (dto) => request({
    url: baseUrl + '/admin/sysUser/update',
    method: 'post',
    data:dto
})

export const add = (dto) => request({
    url: baseUrl + '/admin/sysUser/add',
    method: 'post',
    data:dto
})

export const listLoad= (dto) => request({
    url: baseUrl + '/admin/sysUser/list',
    method: 'post',
    data:dto
})

export const userPositionAdd= (dto) => request({
    url: baseUrl + '/admin/sysUser/userPositionAdd',
    method: 'post',
    data:dto
})


