import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const loginByUsername = (username, password, code, redomStr) => request({
    url: baseUrl + '/admin/user/pcLogin',
    method: 'POST',
    data: {
        username,
        password,
        code,
        redomStr
    }
})
export const userRegister = (dto) => request({
    url: baseUrl + '/admin/user/register',
    method: 'POST',
    data: dto
})
export const getUserInfo = () => request({
    url: baseUrl + '/admin/user/getUserInfo',
    method: 'POST'
});

export const RefeshToken = () => request({
    url: baseUrl + '/admin/user/refesh',
    method: 'POST'
})

export const getTopMenu = () => request({
    url: baseUrl + '/admin/sysMenu/getTopMenu',
    method: 'POST'
});

export const getMenu = (type = 1) => request({
    url: baseUrl + '/admin/sysMenu/getMenu/' + type,
    method: 'POST'
});


export const getMenuAll = () => request({
    url: baseUrl + '/admin/sysMenu/getMenuAll',
    method: 'POST'
});

export const getTableData = (page) => request({
    url: '/user/getTable',
    method: 'get',
    data: {
        page
    }
});

export const logout = () => request({
    url: '/user/logout',
    method: 'get'
})
