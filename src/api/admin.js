import request from '@/router/axios';
import {baseUrl} from '@/config/env';
export const getUserData = (page) => request({
    url: baseUrl+'/admin/user/list',
    method: 'POST',
    data: page
})

export const getRoleData = (page) => request({
    url:baseUrl+ '/admin/sysRole/list',
    method: 'POST',
    data: page
})



export const getDic = (type) => request({
    url: '/admin/getDic',
    method: 'get',
    data: {
        type
    }
})
