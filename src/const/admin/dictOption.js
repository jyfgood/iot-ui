export const dictOption = {

    column: [

        {
            tree: true,
            title: "字典名称",
            key: "name",
            width: "200"
        },
        {
            title: "字典编码",
            key: "code",
            width: "150",
        },
        {
            title: "字典值",
            key: "value",
            width: "150",
        },
        {
            title: "使用状态",
            key: "stateName",
            width: "150",
        },
        {
            title: "字典类型",
            key: "typeName",
            width: "150",
        },
        {
            title: "创建时间",
            key: "createTime",
            width: "200",

        }, {
            title: '操作',
            slot: 'action',
            align: 'center'
        }
    ]
};
export const dictFormOption = {

    column: [
        {
            label: "字典名称",
            prop: "name",
            span: 6
        },

        {
            label: "字典值",
            prop: "value",
            span: 6
        },
        {
            label: "使用状态",
            prop: "state",
            span: 6,
            type: 'select',
            dataType: 'number',
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],

        },
        {
            label: "类型",
            prop: "type",
            span: 6,
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            dicData: [
                {
                    name: "系统",
                    code: 0
                },
                {
                    name: "用户",
                    code: 1
                },
            ]
        }
    ]
};
export const dictFormModelOption = {
    submitBtn: false,
    emptyBtn: false,
    column: [
        {
            label: "字典名称",
            prop: "name",
            rules: [
                {
                    required: true,
                    message: '请选择字典名称',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "字典编码",
            prop: "code",
            rules: [
                {
                    required: true,
                    message: '请输入字典编码',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "字典值",
            prop: "value",

        },
        {
            label: "使用状态",
            prop: "state",
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],
            rules: [
                {
                    required: true,
                    message: '请选择使用状态',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "类型",
            prop: "type",
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            dicData: [
                {
                    name: "系统",
                    code: 0
                },
                {
                    name: "用户",
                    code: 1
                },
            ],
            rules: [
                {
                    required: true,
                    message: '请选择类型',
                    trigger: 'blur'
                },
            ]
        }
    ]
};
