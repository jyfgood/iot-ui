import {
    DIC
} from '@/const/dic'

export const userOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    selection: false,
    menuBtn: true,
    align: 'center',
    menuAlign: 'center',
    // dicData: DIC,
    column: [
        {
            label: "用户名",
            prop: "username",
            width: "150",
            fixed: true,
            rules: [{
                required: true,
                message: "请输入用户名",
                trigger: "blur"
            }]
        },
        {
            label: "真实姓名",
            prop: "realName",
            search: true,
            rules: [{
                required: true,
                message: "请输入真实姓名",
                trigger: "blur"
            }]
        },
        {
            label: "性别",
            prop: "sex",
            type: "radio",
            search: true,
            props: {
                label: 'name',
                value: 'code'
            },
            fixed: true,
            dicData: [
                {
                    name: "女",
                    code: 0
                },
                {
                    name: "男",
                    code: 1
                },
            ],
            rules: [{
                required: true,
                message: "请选择性别",
                trigger: "blur"
            }]
        },
        {
            label: "身份证号",
            prop: "idCard",
            search: true,
            rules: [{
                required: true,
                message: "请输入真实姓名",
                trigger: "blur"
            }]
        },
        {
            label: "账号状态",
            prop: "state",
            type: "select",
            props: {
                label: 'name',
                value: 'code'
            },
            fixed: true,
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],
        },
        {
            label: "邮箱",
            prop: "email"
        },
        {
            label: "手机号",
            prop: "phoneNumber",
            search: true,
            rules: [{
                required: true,
                message: "请输入真实姓名",
                trigger: "blur"
            }]
        },

        {
            label: "创建时间",
            prop: "createTime",
            type: "datetime",
            hide: true,
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
        },
        {
            label: "修改时间",
            prop: "updateTime",
            type: "datetime",
            hide: true,
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
        }
    ]
};
export const roleOption = {
    border: true,
    index: true,
    selection: false,
    calcHeight: 320,
    menuBtn: true,
    menuAlign: 'center',
    menuWidth: 320,
    column: [
        {
            label: "角色名称",
            prop: "name",
            search:true,
            width: "150",
            fixed: true,
            rules: [{
                required: true,
                message: "请输入用户名",
                trigger: "blur"
            }]
        },
        {
            label: "角色编码",
            prop: "code",
            width: "150",
            search:true,
            fixed: true,
            rules: [{
                required: true,
                message: "请输入角色编码",
                trigger: "blur"
            }]
        },
        {
            label: "角色描述",
            prop: "des",
            width: "150",
            fixed: true,
        },
        {
            label: "创建时间",
            prop: "createTime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            type: "date",
        }
    ]
};

export const positionOption = {
    border: true,
    index: true,
    selection: false,
    calcHeight: 320,
    menuBtn: true,
    menuAlign: 'center',
    menuWidth: 320,
    column: [
        {
            label: "岗位名称",
            prop: "name",
            search:true,
            width: "150",
            fixed: true,
            rules: [{
                required: true,
                message: "请输入岗位名称",
                trigger: "blur"
            }]
        },
        {
            label: "别名",
            prop: "alias",
            search:true,
            width: "150",
            fixed: true,
        },
        {
            label: "职责",
            prop: "duty",
            width: "150",
            fixed: true
        },
        {
            label: "状态",
            prop: "state",
            search:true,
            width: "150",
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            fixed: true,
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],
            rules: [
                {
                    required: true,
                    message: '请选择状态',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "创建时间",
            prop: "createTime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            type: "date",
            rules: [
                {
                    required: true,
                    message: '请选择创建时间',
                    trigger: 'blur'
                },
            ]
        }
    ]
};

export const deptOption = {

    column: [

        {
            tree: true,
            title: "部门名称",
            key: "name",
            width: "200"
        },
        {
            title: "别名",
            key: "alias",
            width: "150",
        },
        {
            title: "编码",
            key: "code",
            width: "150",
        },
        {
            title: "联系电话",
            key: "telephone",
            width: "150",
        },
        {
            title: "类型",
            key: "type",
            width: "150",
        },
        {
            title: "创建时间",
            key: "createTime",
            width: "200",

        }, {
            title: '操作',
            slot: 'action',
            align: 'center'
        }
    ]
};
export const deptFormOption = {

    column: [
        {
            label: "部门名称",
            prop: "name",
            span: 6
        },
        {
            label: "别名",
            prop: "alias",
            span: 6
        },
        {
            label: "编码",
            prop: "code",
            span: 6
        },
        {
            label: "联系电话",
            prop: "telephone",
            span: 6
        },
        {
            label: "类型",
            prop: "typeCode",
            span: 6,
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ]
        }
    ]
};
export const deptFormModelOption = {
    submitBtn: false,
    emptyBtn: false,
    column: [
        {
            label: "部门名称",
            prop: "name",
            rules: [
                {
                    required: true,
                    message: '请选择创建时间',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "别名",
            prop: "alias"
        },
        {
            label: "编码",
            prop: "code",
            rules: [
                {
                    required: true,
                    message: '请选择创建时间',
                    trigger: 'blur'
                },
            ]
        },
        {
            label: "联系电话",
            prop: "telephone",
        },
        {
            label: "类型",
            prop: "typeCode",
            type: 'select',
            dataType: 'number',
            props: {
                label: 'name',
                value: 'code'
            },
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],
            rules: [
                {
                    required: true,
                    message: '请选择创建时间',
                    trigger: 'blur'
                },
            ]
        }
    ]
};
