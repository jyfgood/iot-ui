export const orderOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    indexWidth:100,
    selection: false,
    menuBtn: true,
    align: 'center',
    height:550,
    menuAlign: 'center',
    column: [
        {
            label: "订单编码",
            prop: "orderCode",
            search: true,
            fixed: true,
            editDisabled: true
        },
        {
            label: "状态",
            prop: "state",
            type: "select",
            props: {
                label: 'name',
                value: 'code'
            },
            fixed: true,
            dicData: [
                {
                    name: "未支付",
                    code: 0
                },
                {
                    name: "已支付",
                    code: 1
                },
            ],
        },
        {
            label: "店铺",
            prop: "shopName"
        },
        {
            label:"支付金额",
            prop:"price"
        }
    ]
};
