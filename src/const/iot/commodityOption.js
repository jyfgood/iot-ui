import {baseUrl} from '@/config/env';
export const commodityOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    selection: false,
    menuBtn: true,
    align: 'center',
    menuAlign: 'center',
    column: [
        {
            label: "ID",
            prop: "id",
            addDisplay:false,
            editDisplay:false,
            width: "150",
            fixed: true
        },
        {
            label: "商品名称",
            prop: "name",
            search: true,
            rules: [{
                required: true,
                message: "请输入商品名称",
                trigger: "blur"
            }]
        },
        {
            label: "描述",
            prop: "describe",
            search: true,
            fixed: true
        },
        {
            label: '标签图片',
            prop: 'imageUrl',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            tip: '选择一张首页的标签图片，且不超过500kb',
            action: baseUrl+'/commodity/io/upload',

        },
        {
            label: "类型",
            prop: "type",
            search: true,
            rules: [{
                required: true,
                message: "请选择类型",
                trigger: "blur"
            }]
        },
        {
            label: "状态",
            prop: "state",
            type: "select",
            props: {
                label: 'name',
                value: 'code'
            },
            fixed: true,
            dicData: [
                {
                    name: "停用",
                    code: 0
                },
                {
                    name: "使用中",
                    code: 1
                },
            ],
        },
        {
            label: "店铺",
            prop: "shopName"
        }
    ]
};
export const commodityModelOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    selection: false,
    menuBtn: true,
    align: 'center',
    menuAlign: 'center',
    // dicData: DIC,
    column: [
        {
            label: "ID",
            prop: "id",
            width: "150",
            fixed: true
        },
        {
            label: "型号名称",
            prop: "name",
            search: true,
            rules: [{
                required: true,
                message: "请输入型号名称",
                trigger: "blur"
            }]
        },
        {
            label: '图片上传组件',
            prop: 'imageUrl',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
                home:'http://demo.cssmoban.com',
                res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
        },
        {
            label: "价格",
            prop: "price",
            search: true,
            fixed: true,
            rules: [{
                required: true,
                message: "请选择价格",
                trigger: "blur"
            }]
        },
        {
            label: "是否打折",
            prop: "discount",
            search: true,
            rules: [{
                required: true,
                message: "请输入真实姓名",
                trigger: "blur"
            }]
        },
        {
            label: "库存数",
            prop: "repertoryNum",
            fixed: true,

        },
        {
            label: "允许的超卖数量",
            prop: "oversoldNum"
        }
    ]
};
export const commodityTypeOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    selection: false,
    menuBtn: true,
    align: 'center',
    menuAlign: 'center',
    // dicData: DIC,
    column: [
        {
            label: "ID",
            prop: "id",
            width: "150",
            fixed: true
        },
        {
            label: "标题",
            prop: "label",
            search: true,
            rules: [{
                required: true,
                message: "请输入标题",
                trigger: "blur"
            }]
        },

        {
            label: "描述",
            prop: "des",
            search: true,
            rules: [{
                required: true,
                message: "请输入描述",
                trigger: "blur"
            }]
        }
    ]
};
